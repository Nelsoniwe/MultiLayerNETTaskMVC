﻿using DAL.Context;
using DAL.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork , IDisposable
    {
        private StoreContext storeContext;
        private ProductRepository productRepository;
        private CustomerRepository customerRepository;
        private OrderRepository orderRepository;
        private StorehouseOrderRepository storehouseOrderRepository;

        /// <summary>
        /// ctor that sets connectionString
        /// </summary>
        /// <param name="connectionString">connectionString to connet to DB</param>
        public UnitOfWork(string connectionString)
        {
            storeContext = new StoreContext(connectionString);
        }

        public IRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(storeContext);
                return productRepository;
            } 
        }
        public IRepository<Customer> Customers
        {
            get
            {
                if (customerRepository == null)
                    customerRepository = new CustomerRepository(storeContext);
                return customerRepository;
            }
        }
        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(storeContext);
                return orderRepository;
            }
        }
        public IRepository<StorehouseOrder> StorehouseOrders
        {
            get
            {
                if (storehouseOrderRepository == null)
                    storehouseOrderRepository = new StorehouseOrderRepository(storeContext);
                return storehouseOrderRepository;
            }
        }


        public void Save()
        {
            storeContext.SaveChanges();
        }

        private bool disposed = false;

        /// <summary>
        /// Dispose func
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    storeContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
