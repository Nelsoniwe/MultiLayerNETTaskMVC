﻿using DAL.Context;
using DAL.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository that works with Product table
    /// </summary>
    public class ProductRepository : IRepository<Product>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly StoreContext context;

        /// <summary>
        /// ctor that sets context
        /// </summary>
        /// <param name="storeContext"></param>
        public ProductRepository(StoreContext storeContext)
        {
            context = storeContext;
        }
        public bool Create(Product item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");
            var productFromDB = Get(item.ProductId);
            if (productFromDB != null)
                return false;

            context.Products.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var a = Get(id);
            if (a == null)
            {
                return false;
            }
            context.Products.Remove(a);
            return true;
        }

        public Product Get(int id)
        {
            return context.Products.Find(id);
        }

        public IEnumerable<Product> GetAll()
        {
            return context.Products;
        }

        public bool Update(Product item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");

            var itemFromDB = Get(item.ProductId);
            if (itemFromDB == null)
                return false;

            itemFromDB.Count = item.Count;
            itemFromDB.ProductName = item.ProductName;
            itemFromDB.ProductPrice = item.ProductPrice;

            return true;
        }

    }
}
