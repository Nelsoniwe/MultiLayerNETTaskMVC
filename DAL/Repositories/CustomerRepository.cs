﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Context;
using DAL.Objects;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository that works with Customer table
    /// </summary>
    public class CustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly StoreContext context;

        /// <summary>
        /// ctor that sets context
        /// </summary>
        /// <param name="storeContext"></param>
        public CustomerRepository(StoreContext storeContext)
        {
            context = storeContext;
        }
        public bool Create(Customer item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item),"Argument Null Exception");
            var customerFromDB = Get(item.CustomerId);
            if (customerFromDB != null)
                return false;

            context.Customers.Add(item);
            return true;
        }
        public bool Delete(int id)
        {
            var a = Get(id);
            if (a == null)
            {
                return false;
            }
            context.Customers.Remove(a);
            return true;
        }
        public Customer Get(int id)
        {
           return context.Customers.Find(id);
        }
        public IEnumerable<Customer> GetAll()
        {
            return context.Customers;
        }
        public bool Update(Customer item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");

            var itemFromDB = Get(item.CustomerId);
            if (itemFromDB == null)
                return false;

            itemFromDB.CustomerEmail = item.CustomerEmail;
            itemFromDB.CustomerName = item.CustomerName;
            return true;
        }
    }
}
