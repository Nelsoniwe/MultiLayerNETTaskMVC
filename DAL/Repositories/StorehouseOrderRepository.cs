﻿using DAL.Context;
using DAL.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository that works with StorehouseOrder table
    /// </summary>
    public class StorehouseOrderRepository : IRepository<StorehouseOrder>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly StoreContext context;

        /// <summary>
        /// ctor that sets context
        /// </summary>
        /// <param name="storeContext"></param>
        public StorehouseOrderRepository(StoreContext storeContext)
        {
            context = storeContext;
        }


        public bool Create(StorehouseOrder item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");

            context.StorehouseOrders.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var a = Get(id);
            if (a == null)
            {
                return false;
            }
            context.StorehouseOrders.Remove(a);
            return true;
        }

        public StorehouseOrder Get(int id)
        {
            return context.StorehouseOrders.Find(id);
        }

        public IEnumerable<StorehouseOrder> GetAll()
        {
            return context.StorehouseOrders;
        }

        public bool Update(StorehouseOrder item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");

            var itemFromDB = Get(item.OrderId);
            if (itemFromDB == null)
                return false;

            itemFromDB.Count = item.Count;
            itemFromDB.ProductId = item.ProductId;
            itemFromDB.Status = item.Status;
           
            return true;
        }

    }
}
