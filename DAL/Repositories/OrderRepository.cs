﻿using DAL.Context;
using DAL.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository that works with Order table
    /// </summary>
    public class OrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly StoreContext context;

        /// <summary>
        /// ctor that sets context
        /// </summary>
        /// <param name="storeContext"></param>
        public OrderRepository(StoreContext storeContext)
        {
            context = storeContext;
        }
        public bool Create(Order item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");
            var customerFromDB = Get(item.OrderId);
            if (customerFromDB != null)
                return false;

            context.Orders.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var a = Get(id);
            if (a == null)
            {
                return false;
            }
            context.Orders.Remove(a);
            return true;
        }

        public Order Get(int id)
        {
            return context.Orders.Include(x => x.Products).ToList().Find(x => x.OrderId == id);
        }

        public IEnumerable<Order> GetAll()
        {
            return context.Orders;
        }

        public bool Update(Order item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Argument Null Exception");

            var itemFromDB = Get(item.OrderId);
            if (itemFromDB == null)
                return false;

            itemFromDB.Products = item.Products;

            return true;
        }
    }
}
