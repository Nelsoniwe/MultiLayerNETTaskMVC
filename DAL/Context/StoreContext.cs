﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Objects;
using Microsoft.EntityFrameworkCore;

namespace DAL.Context
{
    public class StoreContext : DbContext
    {
        private string connectionString { get; set; }
        //@"Server=localhost,1433;Database=NewDataBase;User ID=sa;Password=Str0ngPa$$w0rd"
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(this.connectionString);
        }

        public StoreContext(string connectionString)
        {
            this.connectionString = connectionString;
            //Database.EnsureDeleted();
            //Database.EnsureCreated();

            //Customer customer1 = new Customer() { CustomerName = "Joe", CustomerEmail = "Joe@gmail.com" };
            //Customer customer2 = new Customer() { CustomerName = "Marine", CustomerEmail = "Marine@gmail.com" };
            //Customer customer3 = new Customer() { CustomerName = "Elya", CustomerEmail = "Elya@gmail.com" };

            //Product product1 = new Product() { ProductName = "phone", ProductPrice = 1300, Count = 41 };
            //Product product2 = new Product() { ProductName = "TV", ProductPrice = 1220, Count = 21 };
            //Product product3 = new Product() { ProductName = "keyboard", ProductPrice = 1000, Count = 23 };

            //Order order1 = new Order() { CustomerId = 1, Products = new List<Product>() { product1 } };
            //Order order2 = new Order() { CustomerId = 1, Products = new List<Product>() { product2, product1 } };
            //Order order3 = new Order() { CustomerId = 1, Products = new List<Product>() { product3, product1, product2 } };

            //StorehouseOrder storehouseOrder1 = new StorehouseOrder() { Count = 10, ProductId = 1, Status = StoreHouseOrderStatus.OnWay };
            //StorehouseOrder storehouseOrder2 = new StorehouseOrder() { Count = 5, ProductId = 2, Status = StoreHouseOrderStatus.OnWay };
            //StorehouseOrder storehouseOrder3 = new StorehouseOrder() { Count = 3, ProductId = 3, Status = StoreHouseOrderStatus.OnWay };

            //Customers.Add(customer1);
            //Customers.Add(customer2);
            //Customers.Add(customer3);

            //Products.Add(product1);
            //Products.Add(product2);
            //Products.Add(product3);

            //Orders.Add(order1);
            //Orders.Add(order2);
            //Orders.Add(order3);

            //StorehouseOrders.Add(storehouseOrder1);
            //StorehouseOrders.Add(storehouseOrder2);
            //StorehouseOrders.Add(storehouseOrder3);

            //SaveChanges();
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<StorehouseOrder> StorehouseOrders { get; set; }
    }
}
