﻿using DAL.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// CRUD Product Repository to work with db
        /// </summary>
        IRepository<Product> Products { get; }
        /// <summary>
        /// CRUD Customer Repository to work with db
        /// </summary>
        IRepository<Customer> Customers { get; }
        /// <summary>
        /// CRUD Order Repository to work with db
        /// </summary>
        IRepository<Order> Orders { get; }
        /// <summary>
        /// CRUD StorehouseOrder Repository to work with db
        /// </summary>
        IRepository<StorehouseOrder> StorehouseOrders { get; }

        /// <summary>
        /// Manualy dispose func
        /// </summary>
        void Dispose();

        /// <summary>
        /// Saves changes in db
        /// </summary>
        void Save();
    }
}
