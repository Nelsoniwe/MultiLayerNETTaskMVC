﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Adds object to db
        /// </summary>
        /// <param name="item">instance of object to add</param>
        /// <returns></returns>
        public bool Create(T item);
        /// <summary>
        /// Returns all objects in table
        /// </summary>
        /// <returns>IEnumerable of such type object instances in db</returns>
        public IEnumerable<T> GetAll();
        /// <summary>
        /// Returns instance of obj
        /// </summary>
        /// <param name="id">id of item in db</param>
        /// <returns>instance of object </returns>
        public T Get(int id);
        /// <summary>
        /// Updates item in DB depends on id
        /// </summary>
        /// <param name="item">instance of object with changed information and old id</param>
        /// <returns>True if info updated, false if it's not</returns>
        public bool Update(T item);
        /// <summary>
        /// Delete object from db
        /// </summary>
        /// <param name="id">id of object</param>
        /// <returns>True if object deleted, false if it's not</returns>
        public bool Delete(int id);
    }
}
