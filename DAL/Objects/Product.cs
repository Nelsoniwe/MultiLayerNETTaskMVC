﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Objects
{
    public class Product
    {
        /// <summary>
        /// The Id of Product
        /// </summary>
        [Key]
        public int ProductId { get; set; }
        /// <summary>
        /// The Name of Product
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// The Price of Product
        /// </summary>
        public decimal ProductPrice { get; set; }
        /// <summary>
        /// The Count of Product in Storehouse
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Orders that have this product
        /// </summary>
        public virtual List<Order> Orders { get; set; }
    }
}
