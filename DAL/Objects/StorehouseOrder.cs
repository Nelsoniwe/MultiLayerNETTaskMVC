﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Objects
{
    public class StorehouseOrder
    {
        /// <summary>
        /// The Id of Order
        /// </summary>
        [Key]
        public int OrderId { get; set; }
        /// <summary>
        /// The Id of Product
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// The instance of product that in storehouse order
        /// </summary>
        public Product product { get; set; }
        /// <summary>
        /// The count of product in storehouse order
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Status of storehouse order
        /// </summary>
        public StoreHouseOrderStatus Status { get; set; } = StoreHouseOrderStatus.OnWay;
    }
}
