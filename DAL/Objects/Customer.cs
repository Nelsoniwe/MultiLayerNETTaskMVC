﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Objects
{
    public class Customer
    {
        /// <summary>
        /// The Id of Cuctomer
        /// </summary>
        [Key]
        public int CustomerId { get; set; }
        /// <summary>
        /// The name of Customer
        /// </summary>
        public string CustomerName { get; set; }
        /// <summary>
        /// The Email of Cuctomer
        /// </summary>
        public string CustomerEmail { get; set; }
    }
}
