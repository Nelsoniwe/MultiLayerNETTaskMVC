﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Objects
{
    public class Order
    {
        /// <summary>
        /// The Id of Order
        /// </summary>
        [Key]
        public int OrderId { get; set; }
        /// <summary>
        /// The Id of Cuctomer
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// The instance of customer who has this order
        /// </summary>
        public Customer customer { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}
