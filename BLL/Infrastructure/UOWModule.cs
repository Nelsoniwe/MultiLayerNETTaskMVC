﻿using Ninject.Modules;
using System;
using DAL.Interfaces;
using DAL.Repositories;
using System.Collections.Generic;
using System.Text;

namespace BLL.Infrastructure
{
    /// <summary>
    /// Class for creating container UOF
    /// </summary>
    public class UOWModule : NinjectModule
    {
        /// <summary>
        /// connectionString for connection to DB
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Ctor for set connectionString
        /// </summary>
        /// <param name="connection">connectionString</param>
        public UOWModule(string connection)
        {
            connectionString = connection;
        }

        /// <summary>
        /// Load containers
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
