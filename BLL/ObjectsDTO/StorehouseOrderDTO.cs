﻿

namespace BLL.ObjectsDTO
{
    public class StorehouseOrderDTO
    {
        /// <summary>
        /// The Id of Order
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// The Id of Product
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// The count of product in storehouse order
        /// </summary>
        public int Count { get; set; }

       
        /// <summary>
        /// Status of storehouse order
        /// </summary>
        public StoreHouseOrderStatusDTO Status { get; set; } = StoreHouseOrderStatusDTO.OnWay;
    }
}
