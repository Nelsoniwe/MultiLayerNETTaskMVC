﻿

namespace BLL.ObjectsDTO
{
    public class CustomerDTO
    {
        /// <summary>
        /// The Id of Cuctomer
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// The name of Customer
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// The Email of Cuctomer
        /// </summary>
        public string CustomerEmail { get; set; }
    }
}
