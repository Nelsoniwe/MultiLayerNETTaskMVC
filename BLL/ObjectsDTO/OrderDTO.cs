﻿

namespace BLL.ObjectsDTO
{
    public class OrderDTO
    {
        /// <summary>
        /// The Id of Order
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// The Id of Cuctomer
        /// </summary>
        public int CustomerId { get; set; }
    }
}
