﻿

namespace BLL.ObjectsDTO
{
    public class ProductDTO
    {
        /// <summary>
        /// The Id of Product
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// The Name of Product
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// The Price of Product
        /// </summary>
        public decimal ProductPrice { get; set; }

        /// <summary>
        /// The Count of Product in Storehouse
        /// </summary>
        public int Count { get; set; }
    }
}
