﻿using DAL.Interfaces;
using DAL.Repositories;
using DAL.Objects;
using BLL.ObjectsDTO;
using BLL.Infrastructure;
using AutoMapper;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;

namespace BLL.Services
{
    public class ProductService : IProductService
    {
        /// <summary>
        /// UOF for work with Repositories
        /// </summary>
        private readonly IUnitOfWork DB;

        /// <summary>
        /// Ctor for set UOF
        /// </summary>
        /// <param name="db">UnitOfWork instance</param>
        public ProductService(IUnitOfWork db)
        {
            DB = db;
        }

        public void AddProductToDB(ProductDTO productDTO)
        {
            if (productDTO == null)
                throw new ArgumentNullException($"{nameof(productDTO)} null exception");
            if (DB.Products.GetAll().FirstOrDefault(x => x.ProductName == productDTO.ProductName) != null)
                throw new ValidationException("Such product already exist", nameof(productDTO));
            if (productDTO.ProductName is null)
                throw new ArgumentNullException($"{nameof(productDTO.ProductName)} null exception");
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, Product>()).CreateMapper();
            var product = mapper.Map<ProductDTO, Product>(productDTO);

            DB.Products.Create(product);
            DB.Save();
        }

        public void UpdateProduct(ProductDTO productDTO)
        {
            if (productDTO == null)
                throw new ArgumentNullException($"{nameof(productDTO)} null exception");
            var allProducts = DB.Products.GetAll();
            if (allProducts.FirstOrDefault(x => x.ProductId == productDTO.ProductId) == null)
                throw new ValidationException("Product doesn't exist", nameof(productDTO));
            if (allProducts.FirstOrDefault(x => x.ProductName == productDTO.ProductName && !(x.ProductId == productDTO.ProductId)) != null)
                throw new ValidationException("Such product already exist", nameof(productDTO));
            productDTO.ProductName ??= allProducts.FirstOrDefault(x => x.ProductId == productDTO.ProductId).ProductName;
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, Product>()).CreateMapper();
            var product = mapper.Map<ProductDTO, Product>(productDTO);

            DB.Products.Update(product);
            DB.Save();
        }

        public void DeleteProduct(ProductDTO productDTO)
        {
            if (productDTO == null)
                throw new ArgumentNullException($"{nameof(productDTO)} null exception");
            var product = DB.Products.GetAll().FirstOrDefault(x => x.ProductId == productDTO.ProductId);
            if (product == null)
                throw new ValidationException("Customer doesn't exist", nameof(productDTO));

            DB.Customers.Delete(product.ProductId);
            DB.Save();
        }

        public void DeleteProductById(int id)
        {
            var product = DB.Products.GetAll().FirstOrDefault(x => x.ProductId == id);
            if (product == null)
                throw new ValidationException("Customer doesn't exist", nameof(id));

            DB.Products.Delete(product.ProductId);
            DB.Save();
        }

        public ProductDTO GetProductByName(string productName)
        {
            if (productName == null)
                throw new ArgumentNullException($"{nameof(productName)} null exception");
            var product = DB.Products.GetAll().FirstOrDefault(x => x.ProductName == productName);
            if (product == null)
                throw new ValidationException("Product doesn't exist", nameof(productName));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDTO>()).CreateMapper();
            return mapper.Map<Product, ProductDTO>(product);
        }

        public IEnumerable<ProductDTO> GetAllProducts()
        {
            var products = DB.Products.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDTO>()).CreateMapper();
            var productsDTO = mapper.Map<IEnumerable<Product>, IEnumerable<ProductDTO>>(products);
            return productsDTO;
        }

        public ProductDTO GetProductById(int id)
        {
            if (id <= 0)
                throw new ArgumentException($"{nameof(id)} must be greater then 0");
            var product = DB.Products.GetAll().FirstOrDefault(x => x.ProductId == id);
            if (product == null)
                return null;

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDTO>()).CreateMapper();
            return mapper.Map<Product, ProductDTO>(product);
        }

        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
