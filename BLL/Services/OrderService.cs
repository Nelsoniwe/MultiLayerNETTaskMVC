﻿using DAL.Interfaces;
using DAL.Repositories;
using DAL.Objects;
using BLL.ObjectsDTO;
using BLL.Infrastructure;
using AutoMapper;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;

namespace BLL.Services
{
    public class OrderService : IOrderService
    {
        /// <summary>
        /// UOF for work with Repositories
        /// </summary>
        private readonly IUnitOfWork DB;

        /// <summary>
        /// Ctor for set UOF
        /// </summary>
        /// <param name="db">UnitOfWork instance</param>
        public OrderService(IUnitOfWork db)
        {
            DB = db;
        }

        public void CreateOrder(OrderDTO orderDTO)
        {
            if (orderDTO == null)
                throw new ArgumentNullException($"{nameof(orderDTO)} null exception");

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDTO, Order>()).CreateMapper();

            DB.Orders.Create(mapper.Map<OrderDTO, Order>(orderDTO));
            DB.Save();
        }

        public void UpdateOrder(OrderDTO orderDTO)
        {
            if (orderDTO == null)
                throw new ArgumentNullException($"{nameof(orderDTO)} null exception");
            if (DB.Orders.GetAll().FirstOrDefault(x => x.OrderId == orderDTO.OrderId) == null)
                throw new ValidationException("Order is not exist", nameof(orderDTO));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDTO, Order>()).CreateMapper();

            var order = mapper.Map<OrderDTO, Order>(orderDTO);

            DB.Orders.Update(order);
            DB.Save();
        }

        public void DeleteOrderById(int orderId)
        {
            var order = DB.Orders.GetAll().FirstOrDefault(x => x.OrderId == orderId);
            if (order == null)
                throw new ValidationException("Order is not exist", nameof(order));

            DB.Orders.Delete(order.OrderId);
            DB.Save();
        }

        public IEnumerable<OrderDTO> GetOrdersByCustomerId(int customerId)
        {
            var orders = DB.Orders.GetAll().Where(x => x.CustomerId == customerId);
            if (orders.Count() == 0)
                throw new ValidationException("Customer hasn't any order", nameof(customerId));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Order>, IEnumerable<OrderDTO>>(orders);
        }
        public IEnumerable<ProductDTO> GetProductsByOrderId(int orderId)
        {
            var order = DB.Orders.Get(orderId);
            if (order == null)
                throw new ValidationException("Order is not found", nameof(orderId));

            if (order.Products == null)
            {
                return new List<ProductDTO>();
            }

            var products = DB.Products.GetAll().Where(x => order.Products.FirstOrDefault(y => y.ProductId == x.ProductId) != null);
            //var allproducts = DB.Products.GetAll();
            //var products = allproducts.Where(x => x.Orders.Where(y => y.OrderId == orderId));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDTO>()).CreateMapper();
            
            var productsDTO = mapper.Map<IEnumerable<Product>, IEnumerable<ProductDTO>>(products);
            return productsDTO;
        }

        public void AddProductToOrder(int productId,int orderId)
        {
            var product = DB.Products.Get(productId);
            var order = DB.Orders.Get(orderId);
            if (product == null)
                throw new ValidationException("Product doesn't exist", nameof(productId));
            if (order == null)
                throw new ValidationException("Order doesn't exist", nameof(orderId));

            if (product.Count == 0)
                throw new ValidationException("This amount of product doesn't exist in storehouse", nameof(orderId));
            if (order.Products == null)
                order.Products = new List<Product>();
            order.Products.Add(product);
            DB.Orders.Update(order);
            DB.Save();
        }

        public void RemoveProductFromOrder(int productId, int orderId)
        {
            var product = DB.Products.Get(productId);
            var order = DB.Orders.Get(orderId);

            if (product == null)
                throw new ValidationException("Product doesn't exist", nameof(productId));
            if (order == null)
                throw new ValidationException("Order doesn't exist", nameof(orderId));

            order.Products.Remove(order.Products.FirstOrDefault(x => x.ProductId == productId));
            DB.Orders.Update(order);
            DB.Save();
        }

        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
