﻿using DAL.Interfaces;
using DAL.Repositories;
using DAL.Objects;
using BLL.ObjectsDTO;
using BLL.Infrastructure;
using AutoMapper;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;


namespace BLL.Services
{
    public class CustomerService : ICustomerService
    {
        /// <summary>
        /// UOF for work with Repositories
        /// </summary>
        private readonly IUnitOfWork DB;

        /// <summary>
        /// Ctor for set UOF
        /// </summary>
        /// <param name="db">UnitOfWork instance</param>
        public CustomerService(IUnitOfWork db)
        {
            DB = db;
        }

        public void AddCustomerToDB(CustomerDTO customerDTO)
        {
            if (customerDTO == null)
                throw new ArgumentNullException($"{nameof(customerDTO)} null exception");
            if (customerDTO.CustomerEmail is null || customerDTO.CustomerEmail == "")
            {
                throw new ArgumentException($"params can't be null",nameof(customerDTO.CustomerEmail));
            }
            if (customerDTO.CustomerName is null || customerDTO.CustomerName == "")
            {
                throw new ArgumentException($"params can't be null", nameof(customerDTO.CustomerName));
            }
            if (DB.Customers.GetAll().FirstOrDefault(x => x.CustomerEmail == customerDTO.CustomerEmail) != null)
                throw new ValidationException("Such customer already exist",nameof(customerDTO));
            
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, Customer>()).CreateMapper();
            var customer = mapper.Map<CustomerDTO, Customer>(customerDTO);

            DB.Customers.Create(customer);
            DB.Save();
        }

        /// <summary>
        /// Should update user Info
        /// </summary>
        /// <param name="customerDTO">instance of CustomerDTO with old id and new params</param>
        public void UpdateCustomer(CustomerDTO customerDTO)
        {
            if (customerDTO == null)
                throw new ArgumentNullException($"{nameof(customerDTO)} null exception");
            if (customerDTO.CustomerEmail is null || customerDTO.CustomerEmail == "")
            {
                throw new ArgumentException($"params can't be null", nameof(customerDTO.CustomerEmail));
            }
            if (customerDTO.CustomerName is null || customerDTO.CustomerName == "")
            {
                throw new ArgumentException($"params can't be null", nameof(customerDTO.CustomerName));
            }
            if (DB.Customers.GetAll().FirstOrDefault(x => x.CustomerEmail == customerDTO.CustomerEmail && x.CustomerId != customerDTO.CustomerId) != null)
                throw new ValidationException("Such customer already exist", nameof(customerDTO));
            if (DB.Customers.GetAll().FirstOrDefault(x => x.CustomerId == customerDTO.CustomerId) == null)
                throw new ValidationException("Customer doesn't exist", nameof(customerDTO));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, Customer>()).CreateMapper();
            var customer = mapper.Map<CustomerDTO, Customer>(customerDTO);

            DB.Customers.Update(customer);
            DB.Save();
        }

        public void DeleteCustomer(CustomerDTO customerDTO)
        {
            if (customerDTO == null)
                throw new ArgumentNullException($"{nameof(customerDTO)} null exception");
            var customer = DB.Customers.GetAll().FirstOrDefault(x => x.CustomerId == customerDTO.CustomerId);
            if (customer == null)
                throw new ValidationException("Customer doesn't exist", nameof(customerDTO));

            DB.Customers.Delete(customer.CustomerId);
            DB.Save();
        }

        public CustomerDTO GetUserByEmail(string email)
        {
            if (email == null)
                throw new ArgumentNullException($"{nameof(email)} null exception");
            var customer = DB.Customers.GetAll().FirstOrDefault(x => x.CustomerEmail == email);
            if (customer == null)
                throw new ValidationException("Customer doesn't exist", nameof(email));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Customer,CustomerDTO>()).CreateMapper();
            return mapper.Map<Customer, CustomerDTO>(customer);
        }
        public CustomerDTO GetUserById(int id)
        {
            var customer = DB.Customers.GetAll().FirstOrDefault(x => x.CustomerId == id);
            if (customer == null)
                throw new ValidationException("Customer doesn't exist", nameof(id));

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDTO>()).CreateMapper();
            return mapper.Map<Customer, CustomerDTO>(customer);
        }
        public void Dispose()
        {
            DB.Dispose();
        }

        public IEnumerable<CustomerDTO> GetAllCustomers()
        {
            var customers = DB.Customers.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDTO>()).CreateMapper();
            var customersDTO = mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerDTO>>(customers);
            return customersDTO;
        }
    }
}
