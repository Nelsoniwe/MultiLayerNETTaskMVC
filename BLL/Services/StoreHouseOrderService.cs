﻿using DAL.Interfaces;
using DAL.Repositories;
using DAL.Objects;
using BLL.ObjectsDTO;
using BLL.Infrastructure;
using AutoMapper;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;

namespace BLL.Services
{
    public class StoreHouseOrderService : IStoreHouseOrderService
    {

        /// <summary>
        /// UOF for work with Repositories
        /// </summary>
        private readonly IUnitOfWork DB;

        /// <summary>
        /// Ctor for set UOF
        /// </summary>
        /// <param name="db">UnitOfWork instance</param>
        public StoreHouseOrderService(IUnitOfWork db)
        {
            DB = db;
        }

        public void AddProductToStoreHouseOrder(ProductDTO productDTO,int count)
        {
            if (productDTO == null)
                throw new ArgumentNullException($"{nameof(productDTO)} null exception");

            var storeHouseOrderDTO = new StorehouseOrderDTO() {
                ProductId = productDTO.ProductId,
                Count = count};

            var config = new MapperConfiguration(cfg => cfg.CreateMap<StorehouseOrderDTO, StorehouseOrder>().ForMember("Status", opt => opt.MapFrom(c => c.Status)));
            var mapper = new Mapper(config);
            var storeHouseOrder = mapper.Map<StorehouseOrderDTO, StorehouseOrder>(storeHouseOrderDTO);
            DB.StorehouseOrders.Create(storeHouseOrder);
            DB.Save();
        }

        public void UpdateStoreHouseOrder(StorehouseOrderDTO storeHouseOrderDTO)
        {
            if (storeHouseOrderDTO == null)
                throw new ArgumentNullException($"{nameof(storeHouseOrderDTO)} null exception");
            if (DB.StorehouseOrders.GetAll().FirstOrDefault(x => x.OrderId == storeHouseOrderDTO.OrderId) == null)
                throw new ValidationException("Storehouse order is not exist", nameof(storeHouseOrderDTO));

            var config = new MapperConfiguration(cfg => cfg.CreateMap<StorehouseOrderDTO, StorehouseOrder>().ForMember("Status", opt => opt.MapFrom(c => c.Status)));
            var mapper = new Mapper(config);
            var storeHouseOrder = mapper.Map<StorehouseOrderDTO, StorehouseOrder>(storeHouseOrderDTO);

            DB.StorehouseOrders.Update(storeHouseOrder);
            DB.Save();
        }

        public void DeleteStoreHouseOrderById(int storeHouseOrderId)
        {
            var order = DB.StorehouseOrders.GetAll().FirstOrDefault(x => x.OrderId == storeHouseOrderId);
            if (order == null)
                throw new ValidationException("Storehouse is not exist", nameof(storeHouseOrderId));

            DB.StorehouseOrders.Delete(order.OrderId);
            DB.Save();
        }

        public IEnumerable<StorehouseOrderDTO> GetAllStoreHouseOrders()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<StorehouseOrder, StorehouseOrderDTO>().ForMember("Status", opt => opt.MapFrom(c => c.Status)));
            var mapper = new Mapper(config);
            var storeHouseOrders = DB.StorehouseOrders.GetAll();
            return mapper.Map<IEnumerable<StorehouseOrder>, IEnumerable<StorehouseOrderDTO>>(storeHouseOrders);
        }

        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
