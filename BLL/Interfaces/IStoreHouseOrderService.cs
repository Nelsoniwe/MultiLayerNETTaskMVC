﻿using BLL.ObjectsDTO;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    /// <summary>
    /// Service to work with storehouse orders
    /// </summary>
    public interface IStoreHouseOrderService
    {
        /// <summary>
        /// Add Storehouse Order to DB
        /// </summary>
        /// <param name="productDTO">Instance of ProductDTO</param>
        /// <param name="count">Count of product to order</param>
        void AddProductToStoreHouseOrder(ProductDTO productDTO, int count);

        /// <summary>
        /// Updates Storehouse order information
        /// </summary>
        /// <param name="storeHouseOrderDTO">Instance of StorehouseOrderDTO</param>
        void UpdateStoreHouseOrder(StorehouseOrderDTO storeHouseOrderDTO);

        /// <summary>
        /// Deletes Storehouse order from DB
        /// </summary>
        /// <param name="storeHouseOrderId">Storehouse order id in DB</param>
        void DeleteStoreHouseOrderById(int storeHouseOrderId);

        /// <summary>
        /// Get all Storehouse orders
        /// </summary>
        /// <returns>IEnumerable of StorehouseOrderDTOs </returns>
        IEnumerable<StorehouseOrderDTO> GetAllStoreHouseOrders();

        /// <summary>
        /// Dispose func
        /// </summary>
        void Dispose();
    }
}
