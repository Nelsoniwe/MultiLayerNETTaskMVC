﻿using BLL.ObjectsDTO;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    /// <summary>
    /// Service to work with customers
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Adds Cusomer to DB
        /// </summary>
        /// <param name="customerDTO">instance of CustomerDTO</param>
        void AddCustomerToDB(CustomerDTO customerDTO);
        /// <summary>
        /// Update Cusomer in DB
        /// </summary>
        /// <param name="customerDTO">instance of CustomerDTO</param>
        void UpdateCustomer(CustomerDTO customerDTO);
        /// <summary>
        /// Delete Cusomer from DB
        /// </summary>
        /// <param name="customerDTO">instance of CustomerDTO</param>
        void DeleteCustomer(CustomerDTO customerDTO);
        /// <summary>
        /// Get User from DB
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns>Instance of CustomerDTO</returns>
        CustomerDTO GetUserByEmail(string email);
        /// <summary>
        /// Get all Users from DB
        /// </summary>
        /// <returns>IEnumerable of CustomerDTO</returns>
        IEnumerable<CustomerDTO> GetAllCustomers();

        /// <summary>
        /// Get User from DB
        /// </summary>
        CustomerDTO GetUserById(int id);
        void Dispose();
    }
}
