﻿using BLL.ObjectsDTO;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    /// <summary>
    /// Service to work with orders
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Adds Order to DB
        /// </summary>
        /// <param name="orderDTO">instance of OrderDTO</param>
        void CreateOrder(OrderDTO orderDTO);

        /// <summary>
        /// Update Order in DB
        /// </summary>
        /// <param name="orderDTO">instance of OrderDTO</param>
        void UpdateOrder(OrderDTO orderDTO);

        /// <summary>
        /// Delete Order from DB
        /// </summary>
        /// <param name="orderId">id of order in DB</param>
        void DeleteOrderById(int orderId);

        /// <summary>
        /// Get all customer orders
        /// </summary>
        /// <param name="customerId">customer id in DB</param>
        /// <returns>IEnumerable of OrderDTO</returns>
        public IEnumerable<OrderDTO> GetOrdersByCustomerId(int customerId);

        /// <summary>
        /// Get all products that attached to the order
        /// </summary>
        /// <param name="orderId">customer id in DB</param>
        /// <returns>IEnumerable of ProductDTO</returns>
        public IEnumerable<ProductDTO> GetProductsByOrderId(int orderId);

        /// <summary>
        /// Adds Product to Order
        /// </summary>
        /// <param name="productId">product id in DB</param>
        /// <param name="orderId">order id in DB</param>
        public void AddProductToOrder(int productId, int orderId);

        /// <summary>
        /// Removes product from Order
        /// </summary>
        /// <param name="productId">product id in DB</param>
        /// <param name="orderId">order id in DB</param>
        public void RemoveProductFromOrder(int productId, int orderId);

        /// <summary>
        /// Dispose func
        /// </summary>
        public void Dispose();
    }
}
