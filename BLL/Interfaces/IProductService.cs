﻿using BLL.ObjectsDTO;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    /// <summary>
    /// Service to work with products
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Adds Product to DB
        /// </summary>
        /// <param name="productDTO">instance of ProductDTO</param>
        void AddProductToDB(ProductDTO productDTO);

        /// <summary>
        /// Update Product in DB
        /// </summary>
        /// <param name="productDTO">instance of ProductDTO</param>
        void UpdateProduct(ProductDTO productDTO);

        /// <summary>
        /// Delete Product from DB
        /// </summary>
        /// <param name="productDTO">instance of productDTO</param>
        void DeleteProduct(ProductDTO productDTO);

        void DeleteProductById(int id);

        /// <summary>
        /// Get Product from DB
        /// </summary>
        /// <param name="productName">product name</param>
        /// <returns>instance of ProductDTO</returns>
        ProductDTO GetProductByName(string productName);

        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns>IEnumerable of ProductDTO</returns>
        public IEnumerable<ProductDTO> GetAllProducts();

        /// <summary>
        /// Get Product from DB
        /// </summary>
        /// <param name="id">product id</param>
        /// <returns>instance of ProductDTO</returns>
        ProductDTO GetProductById(int id);

        /// <summary>
        /// Dispose func
        /// </summary>
        void Dispose();
    }
}
