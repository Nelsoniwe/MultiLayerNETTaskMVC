﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using API.Infrastructure;
using API.Models;
using AutoMapper;
using BLL.ObjectsDTO;

namespace API.Controllers
{
    [Route("Store/[controller]")]
    [ApiController]
    public class StoreHouseOrderController : Controller
    {
        private readonly IStoreHouseOrderService storeHouseOrderService;
        private readonly IProductService productService;
        public StoreHouseOrderController()
        {
            storeHouseOrderService = Conf.GetService(typeof(IStoreHouseOrderService)) as IStoreHouseOrderService;
            productService = Conf.GetService(typeof(IProductService)) as IProductService;
        }

        /// <summary>
        /// Request for getting all StoreHouse Orders
        /// </summary>
        /// <returns>Json string with storeHouseOrdersView objects</returns>
        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                var storeHouseOrdersDTO = storeHouseOrderService.GetAllStoreHouseOrders();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<StorehouseOrderDTO, StorehouseOrderView>().ForMember("Status", opt => opt.MapFrom(c => c.Status))).CreateMapper();
                var storeHouseOrdersView = mapper.Map<IEnumerable<StorehouseOrderDTO> , IEnumerable<StorehouseOrderView>>(storeHouseOrdersDTO);
                return Ok(storeHouseOrdersView);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for creating StoreHouseOrder
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="count">Count of Product to Order</param>
        /// <returns>Status Code</returns>
        [HttpPost("Create/{id}/{count}")]
        public IActionResult CreateOrder(int id,int count)
        {
            try
            {
                var product = productService.GetProductById(id);
                storeHouseOrderService.AddProductToStoreHouseOrder(product, count);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for deleting StoreHouseOrder
        /// </summary>
        /// <param name="id">StoreHouseOrder Id</param>
        /// <returns>Status Code</returns>
        [HttpDelete("Delete/{id}")]
        public IActionResult CreateOrder(int id)
        {
            try
            {
                storeHouseOrderService.DeleteStoreHouseOrderById(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for updating StoreHouseOrder
        /// </summary>
        /// <param name="storehouseOrderView">storehouseOrderView in Json</param>
        /// <returns>Status Code</returns>
        [HttpPut("Update")]
        public IActionResult CreateOrder([FromBody]StorehouseOrderView storehouseOrderView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<StorehouseOrderDTO, StorehouseOrderView>().ForMember("Status", opt => opt.MapFrom(c => c.Status))).CreateMapper();
                var storeHouseOrdersDTO = mapper.Map<StorehouseOrderView, StorehouseOrderDTO>(storehouseOrderView);
                storeHouseOrderService.UpdateStoreHouseOrder(storeHouseOrdersDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
