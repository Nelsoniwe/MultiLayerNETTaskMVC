﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using API.Infrastructure;
using API.Models;
using AutoMapper;
using BLL.ObjectsDTO;

namespace API.Controllers
{
    [ApiController]
    [Route("Store/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;

        public CustomerController()
        {
            customerService = Conf.GetService(typeof(ICustomerService)) as ICustomerService;
        }

        /// <summary>
        /// Request for getting all Customers
        /// </summary>
        /// <returns>Json string with CustomerView objects</returns>
        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                var customers = customerService.GetAllCustomers();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
                var customersView = mapper.Map<IEnumerable<CustomerDTO>, IEnumerable<CustomerView>>(customers);
                return Ok(customersView);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for getting Customer
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns>Json string with CustomerView object</returns>
        [HttpGet("Get/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var customer = customerService.GetUserById(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
                var customerView = mapper.Map<CustomerDTO, CustomerView>(customer);
                return Ok(customerView);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for adding Customer to DB
        /// </summary>
        /// <param name="customerView">customerView info in Json</param>
        /// <returns>Status code</returns>
        [HttpPost("Add")]
        public IActionResult Add(CustomerView customerView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerView, CustomerDTO>()).CreateMapper();
                var customersDTO = mapper.Map<CustomerView, CustomerDTO>(customerView);
                customerService.AddCustomerToDB(customersDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for deleting Customer from DB
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns>Status code</returns>
        [HttpDelete("Delete/{id}")]
        public IActionResult DeleteCustomer(int id)
        {
            try
            {
                var customersDTO = customerService.GetUserById(id);
                customerService.DeleteCustomer(customersDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for updating Customer in DB
        /// </summary>
        /// <param name="customerView">customerView info in Json</param>
        /// <returns>Status Code</returns>
        [HttpPut("Update")]
        public IActionResult EditCustomerFields([FromBody]CustomerView customerView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerView, CustomerDTO>()).CreateMapper();
                var customerDTO = mapper.Map<CustomerView, CustomerDTO>(customerView);
                customerService.UpdateCustomer(customerDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
