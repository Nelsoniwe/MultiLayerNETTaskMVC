﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using API.Infrastructure;
using API.Models;
using AutoMapper;
using BLL.ObjectsDTO;
using BLL.Services;

namespace PLMVC.Controllers
{
    [ApiController]
    [Route("Store/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService productsService;

        public ProductController()
        {
            productsService = Conf.GetService(typeof(ProductService)) as ProductService;
        }

        /// <summary>
        /// Request for getting all Products
        /// </summary>
        /// <returns>Json string with ProductView objects</returns>
        [HttpGet("GetAll")]
        public ActionResult GetAll()
        {
            try
            {
                var products = productsService.GetAllProducts();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, ProductView>()).CreateMapper();
                var productsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(products);
                return Ok(productsView);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for getting Product
        /// </summary>
        /// <param name="name">Product name</param>
        /// <returns>Json string with productView object</returns>
        [HttpGet("Get/{name}")]
        public ActionResult Get(string name)
        {
            try
            {
                var product = productsService.GetProductByName(name);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, ProductView>()).CreateMapper();
                var productView = mapper.Map<ProductDTO, ProductView>(product);
                return Ok(productView);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for adding Product to DB
        /// </summary>
        /// <param name="productView">productView in Json</param>
        /// <returns>Status Code</returns>
        [HttpPost("Add")]
        public IActionResult Add([FromBody]ProductView productView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductView, ProductDTO>()).CreateMapper();
                var productDto = mapper.Map<ProductView, ProductDTO>(productView);
                productsService.AddProductToDB(productDto);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for deleting Product from DB
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <returns>Status Code</returns>
        [HttpDelete("Delete/{id}")]
        public ActionResult DeleteProduct(int id)
        {
            try
            {
                productsService.DeleteProductById(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for updating Product info
        /// </summary>
        /// <param name="productView">productView in Json</param>
        /// <returns>Status Code</returns>
        [HttpPut("Edit")]
        public ActionResult EditProductFields([FromBody]ProductView productView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductView, ProductDTO>()).CreateMapper();
                var productDTO = mapper.Map<ProductView, ProductDTO>(productView);
                productsService.UpdateProduct(productDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
