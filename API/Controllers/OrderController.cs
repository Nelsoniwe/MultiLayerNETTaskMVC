﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using API.Infrastructure;
using API.Models;
using AutoMapper;
using BLL.ObjectsDTO;

namespace PLMVC.Controllers
{
    [ApiController]
    [Route("Store/[controller]")]
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;
        private readonly IProductService productService;
        public OrderController()
        {
            orderService = Conf.GetService(typeof(IOrderService)) as IOrderService;
            productService = Conf.GetService(typeof(IProductService)) as IProductService;
        }

        /// <summary>
        /// Request for getting Customer Orders
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns>Json string with OrderView objects</returns>
        [HttpGet("Get/{id}")]
        public IActionResult GetCustomerOrders(int id)
        {
            try
            {
                var orders = orderService.GetOrdersByCustomerId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDTO, OrderView>();
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();
                var orderViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(orders);

                for (int i = 0; i < orderViews.Count(); i++)
                {
                    var products = orderService.GetProductsByOrderId(orderViews.ToList()[i].OrderId);
                    orderViews.ToList()[i].OrderProducts = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(products);
                }

                return Ok(orderViews);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for creating Customer Orders
        /// </summary>
        /// <param name="order">OrderView in Json</param>
        /// <returns>Status code</returns>
        [HttpPost("Create")]
        public IActionResult CreateOrder([FromBody]OrderView order)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDTO, OrderView>();
                    cfg.CreateMap<OrderView, OrderDTO>();
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();
                var orderDTO = mapper.Map<OrderView, OrderDTO>(order);
                orderService.CreateOrder(orderDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for deleting Customer Orders
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns>Status code</returns>
        [HttpDelete("Delete/{id}")]
        public IActionResult CreateOrder(int id)
        {
            try
            {
                orderService.DeleteOrderById(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for deleting product from order
        /// </summary>
        /// <param name="orderId">order Id</param>
        /// <param name="productId">product Id</param>
        /// <returns>Status code</returns>
        [HttpPut("RemoveProduct/{orderId}/{productId}")]
        public IActionResult DeleteProductFromOrder(int orderId, int productId)
        {
            try
            {
                orderService.RemoveProductFromOrder(productId, orderId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Request for adding product to order
        /// </summary>
        /// <param name="orderId">order Id</param>
        /// <param name="productId">product Id</param>
        /// <returns>Status code</returns>
        [HttpPut("AddProduct/{orderId}/{productId}")]
        public IActionResult AddProductToOrder(int orderId, int productId)
        {
            try
            {
                orderService.AddProductToOrder(productId, orderId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
