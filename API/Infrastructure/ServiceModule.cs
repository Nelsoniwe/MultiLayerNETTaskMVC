﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;

namespace API.Infrastructure
{
    class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOrderService>().To<OrderService>();
            Bind<IProductService>().To<ProductService>();
            Bind<ICustomerService>().To<CustomerService>();
            Bind<IStoreHouseOrderService>().To<StoreHouseOrderService>();
        }
    }
}
