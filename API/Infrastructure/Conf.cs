﻿using System;
using BLL.Infrastructure;
using Ninject;
using Ninject.Modules;

namespace API.Infrastructure
{
    public static class Conf
    {
        private static string ConnectionString;

        private static NinjectModule ninjectModule;
        private static NinjectModule serviceModule;
        private static StandardKernel kernel;
        public static void CreateContainer(string connection)
        {
            ConnectionString = connection;
            ninjectModule = new UOWModule(ConnectionString);
            serviceModule = new ServiceModule();
            kernel = new StandardKernel(ninjectModule, serviceModule);
        }

        public static object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
    }
}
