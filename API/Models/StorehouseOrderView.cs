﻿namespace API.Models
{
    public class StorehouseOrderView
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
        public StoreHouseOrderStatusView Status { get; set; } = StoreHouseOrderStatusView.OnWay;
    }
}
