﻿using System.Collections.Generic;

namespace API.Models
{
    public class OrderView
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public IEnumerable<ProductView> OrderProducts { get; set; }
    }
}
