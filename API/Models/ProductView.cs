﻿namespace API.Models
{
    public class ProductView
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public int Count { get; set; }
    }
}
