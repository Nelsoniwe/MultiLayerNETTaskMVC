﻿using System;
using System.Collections.Generic;
using System.Text;
using MultiLayerNETTask.Interfaces;
using MultiLayerNETTask.ObjectsView;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using BLL.ObjectsDTO;
using BLL.Services;
using MultiLayerNETTask.Infrastructure;
using BLL.Interfaces;

namespace MultiLayerNETTask.Actions
{
    public class CustomerController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        ICustomerService customerService = Conf.GetService(typeof(ICustomerService)) as ICustomerService;

        public CustomerController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = AddCustomer,
                [2] = UpdateCustomerInfo,
                [3] = DeleteCustomerInfo,
                [4] = GetUserInfoByEmail,
                [5] = GetAllCustomers,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Add customer" +
                "\n2 - Update customer info" +
                "\n3 - Delete customer" +
                "\n4 - Get customer info by Email" +
                "\n5 - Get all customers" +
                "\n6 - Return");
        }

        public IController AddCustomer()
        {
            try
            {
                CustomerView newCustomer = new CustomerView();
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }

                Console.Write("Enter customer Name: ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("Wrong Name. Try Again");
                    return this;
                }
                newCustomer.CustomerEmail = email;
                newCustomer.CustomerName = name;

                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerView, CustomerDTO>()).CreateMapper();
                var customerDTO = mapper.Map<CustomerView, CustomerDTO>(newCustomer);
                customerService.AddCustomerToDB(customerDTO);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController UpdateCustomerInfo()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<CustomerDTO, CustomerView>();
                    cfg.CreateMap<CustomerView, CustomerDTO>();
                }).CreateMapper();


                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                var customerDTO = customerService.GetUserByEmail(email);

                var customerView = mapper.Map<CustomerDTO, CustomerView>(customerDTO);

                Console.WriteLine("Enter new name, else enter \"0\"");
                var newValue = Console.ReadLine();
                if (!(newValue == "0" || newValue == ""))
                    customerView.CustomerName = newValue;
                Console.WriteLine("Enter new email, else enter \"0\"");
                newValue = Console.ReadLine();
                if (newValue != "0" || !new EmailAddressAttribute().IsValid(email))
                    customerView.CustomerEmail = newValue;

                customerService.UpdateCustomer(mapper.Map<CustomerView, CustomerDTO>(customerView));
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteCustomerInfo()
        {
            try
            {
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }

                var customerDTO = customerService.GetUserByEmail(email);
                customerService.DeleteCustomer(customerDTO);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController GetUserInfoByEmail()
        {
            try
            {
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }

                var customerDTO = customerService.GetUserByEmail(email);

                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
                var customerView = mapper.Map<CustomerDTO, CustomerView>(customerDTO);

                Console.WriteLine("ID: " + customerView.CustomerId);
                Console.WriteLine("Email: " + customerView.CustomerEmail);
                Console.WriteLine("Name: " + customerView.CustomerName);
                Console.WriteLine();
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController GetAllCustomers()
        {
            try
            {

                var customersDTO = customerService.GetAllCustomers();

                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
                var customersView = mapper.Map<IEnumerable<CustomerDTO>, IEnumerable<CustomerView>>(customersDTO);

                foreach (var item in customersView)
                {
                    Console.WriteLine("ID: " + item.CustomerId);
                    Console.WriteLine("Email: " + item.CustomerEmail);
                    Console.WriteLine("Name: " + item.CustomerName);
                    Console.WriteLine();
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }

    }
}
