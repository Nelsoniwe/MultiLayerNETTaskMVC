﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using BLL.Interfaces;
using BLL.ObjectsDTO;
using MultiLayerNETTask.Infrastructure;
using MultiLayerNETTask.Interfaces;
using MultiLayerNETTask.ObjectsView;

namespace MultiLayerNETTask.Actions
{
    public class StoreHouseOrderController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        IStoreHouseOrderService storeHouseOrderService = Conf.GetService(typeof(IStoreHouseOrderService)) as IStoreHouseOrderService;
        IProductService productService = Conf.GetService(typeof(IProductService)) as IProductService;
        public StoreHouseOrderController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = AddProductToStoreHouseOrder,
                [2] = UpdateStoreHouseOrder,
                [3] = DeleteStoreHouseOrderById,
                [4] = GetAllStoreHouseOrders,
                [5] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Create order" +
                "\n2 - Update order" +
                "\n3 - Delete order" +
                "\n4 - Get all orders" +
                "\n5 - Return");
        }

        IController AddProductToStoreHouseOrder()
        {
            try
            {
                Console.Write("Enter product name: ");
                var name = Console.ReadLine();
                var productDTO = productService.GetProductByName(name);
                if (productDTO == null)
                {
                    Console.WriteLine("Product doesn't exist\n" +
                       "Try Again");
                    return this;
                }

                Console.Write("Enter count to order: ");
                int countToOrder;
                if (!int.TryParse(Console.ReadLine(), out countToOrder) || countToOrder < 0)
                {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                }

                storeHouseOrderService.AddProductToStoreHouseOrder(productDTO, countToOrder);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        IController UpdateStoreHouseOrder()
        {
            try
            {
                var allOrdersDTO = storeHouseOrderService.GetAllStoreHouseOrders();
                var allProductsDTO = productService.GetAllProducts();

                var config = new MapperConfiguration(cfg => { 
                    cfg.CreateMap<StorehouseOrderDTO, StorehouseOrderView>().ForMember("Status", opt => opt.MapFrom(c => c.Status));
                    cfg.CreateMap<StorehouseOrderView, StorehouseOrderDTO>().ForMember("Status", opt => opt.MapFrom(c => c.Status));
                    cfg.CreateMap<ProductDTO, ProductView>();
                    });

                var mapper = new Mapper(config);
                var allOrdersView = mapper.Map<IEnumerable<StorehouseOrderDTO>, IEnumerable<StorehouseOrderView>>(allOrdersDTO);
                var allProductsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(allProductsDTO);

                Dictionary<int, StorehouseOrderView> orderDict = new Dictionary<int, StorehouseOrderView>();
                int counter = 1;
                foreach (var item in allOrdersView)
                {
                    Console.WriteLine($"{counter}) Product: {allProductsView.FirstOrDefault(x => x.ProductId == item.ProductId).ProductName} Count:{item.Count} Status:{item.Status}");
                    orderDict.Add(counter, item);
                    counter++;
                }
                Console.Write("Choose order to Update: ");
                int choose;
                if (!int.TryParse(Console.ReadLine(), out choose) || !orderDict.ContainsKey(choose))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }

                var orderToChange = orderDict[choose];
                Console.WriteLine("Enter new count, else enter \"skip\"");
                var newValue = Console.ReadLine();
                int newIntCountValue = 0;
                if (newValue != "skip")
                {
                    if (!int.TryParse(newValue, out newIntCountValue) || newIntCountValue < 0)
                    {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                    }

                    orderToChange.Count = newIntCountValue;
                }


                Dictionary<int, string> statusDict = new Dictionary<int, string>();
                foreach (StoreHouseOrderStatusView foo in Enum.GetValues(typeof(StoreHouseOrderStatusView)))
                {
                    statusDict.Add((int)foo, foo.ToString());
                }

                foreach (var item in statusDict)
                {
                    Console.WriteLine($"{item.Key}) {item.Value}");
                }
                Console.WriteLine("Enter new status, else enter \"skip\"");
                newValue = Console.ReadLine();
                var newStatusValue = 0;

                if (newValue != "skip")
                {
                    if (!int.TryParse(newValue, out newStatusValue) || !statusDict.ContainsKey(newStatusValue))
                    {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                    }

                    orderToChange.Status = (StoreHouseOrderStatusView)newStatusValue;
                }

                storeHouseOrderService.UpdateStoreHouseOrder(mapper.Map<StorehouseOrderView,StorehouseOrderDTO>(orderToChange));
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        IController DeleteStoreHouseOrderById()
        {
            try
            {
                var allOrdersDTO = storeHouseOrderService.GetAllStoreHouseOrders();
                var allProductsDTO = productService.GetAllProducts();

                var config = new MapperConfiguration(cfg => {
                    cfg.CreateMap<StorehouseOrderDTO, StorehouseOrderView>().ForMember("Status", opt => opt.MapFrom(c => c.Status));
                    cfg.CreateMap<ProductDTO, ProductView>();
                });

                var mapper = new Mapper(config);
                var allOrdersView = mapper.Map<IEnumerable<StorehouseOrderDTO>, IEnumerable<StorehouseOrderView>>(allOrdersDTO);
                var allProductsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(allProductsDTO);

                Dictionary<int, StorehouseOrderView> orderDict = new Dictionary<int, StorehouseOrderView>();
                int counter = 1;

                if (allOrdersView.Count() == 0)
                {
                    Console.WriteLine("Storehouse order is empty");
                    return this;
                }

                foreach (var item in allOrdersView)
                {
                    Console.WriteLine($"{counter}) Product: {allProductsView.FirstOrDefault(x => x.ProductId == item.ProductId).ProductName} Count:{item.Count} Status:{item.Status}");
                    orderDict.Add(counter, item);
                    counter++;
                }
                Console.Write("Choose order to Delete: ");
                int choose;
                if (!int.TryParse(Console.ReadLine(), out choose) || !orderDict.ContainsKey(choose))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }

                storeHouseOrderService.DeleteStoreHouseOrderById(orderDict[choose].OrderId);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        IController GetAllStoreHouseOrders()
        {
            try
            {
                var allOrdersDTO = storeHouseOrderService.GetAllStoreHouseOrders();
                var allProductsDTO = productService.GetAllProducts();

                var config = new MapperConfiguration(cfg => {
                    cfg.CreateMap<StorehouseOrderDTO, StorehouseOrderView>().ForMember("Status", opt => opt.MapFrom(c => c.Status));
                    cfg.CreateMap<ProductDTO, ProductView>();
                });

                var mapper = new Mapper(config);
                var allOrdersView = mapper.Map<IEnumerable<StorehouseOrderDTO>, IEnumerable<StorehouseOrderView>>(allOrdersDTO);
                var allProductsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(allProductsDTO);

                Dictionary<int, StorehouseOrderView> orderDict = new Dictionary<int, StorehouseOrderView>();
                int counter = 1;

                if (allOrdersView.Count() == 0)
                {
                    Console.WriteLine("Storehouse order is empty");
                    return this;
                }

                foreach (var item in allOrdersView)
                {
                    Console.WriteLine($"{counter}) Product: {allProductsView.FirstOrDefault(x => x.ProductId == item.ProductId).ProductName} Count:{item.Count} Status:{item.Status}");
                    orderDict.Add(counter, item);
                    counter++;
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }
    }
}
