﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using AutoMapper;
using BLL.Interfaces;
using BLL.ObjectsDTO;
using MultiLayerNETTask.Infrastructure;
using MultiLayerNETTask.Interfaces;
using MultiLayerNETTask.ObjectsView;

namespace MultiLayerNETTask.Actions
{
    public class OrderController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        IOrderService orderService = Conf.GetService(typeof(IOrderService)) as IOrderService;
        ICustomerService customerService = Conf.GetService(typeof(ICustomerService)) as ICustomerService;
        IProductService productService = Conf.GetService(typeof(IProductService)) as IProductService;
        public OrderController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = CreateNewOrder,
                [2] = DeleteProductFromOrder,
                [3] = DeleteOrder,
                [4] = GetAllCustomerOrders,
                [5] = AddNewProductToOrder,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Create order" +
                "\n2 - Delete product from order" +
                "\n3 - Delete order" +
                "\n4 - Get customer orders by email" +
                "\n5 - Add product to order" +
                "\n6 - Return");
        }

        public IController CreateNewOrder()
        {
            try
            {
                CustomerView newCustomer = new CustomerView();
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                newCustomer.CustomerEmail = email;
                var mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CustomerView, CustomerDTO>(); cfg.CreateMap<OrderView, OrderDTO>(); }).CreateMapper();
                var customerDTO = mapper.Map<CustomerView, CustomerDTO>(newCustomer);
                var customerDTOFromDB = customerService.GetUserByEmail(customerDTO.CustomerEmail);
                OrderView orderView = new OrderView();
                orderView.CustomerId = customerDTOFromDB.CustomerId;

                orderService.CreateOrder(mapper.Map<OrderView, OrderDTO>(orderView));
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteProductFromOrder()
        {
            try
            {
                CustomerView newCustomer = new CustomerView();
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                newCustomer.CustomerEmail = email;
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<CustomerView, CustomerDTO>();
                    cfg.CreateMap<OrderDTO, OrderView>();
                }).CreateMapper();

                var customerDTO = mapper.Map<CustomerView, CustomerDTO>(newCustomer);
                var customerDTOFromDB = customerService.GetUserByEmail(customerDTO.CustomerEmail);

                var ordersDTOFromDB = orderService.GetOrdersByCustomerId(customerDTOFromDB.CustomerId);
                var ordersViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(ordersDTOFromDB);

                int counter = 1;
                var orderDict = new Dictionary<int, OrderView>();
                foreach (var item in ordersViews)
                {
                    var products = orderService.GetProductsByOrderId(item.OrderId);
                    if (products.Count() != 0)
                    {
                        orderDict.Add(counter, item);
                        Console.Write($"{counter})    ");
                        foreach (var product in products)
                        {
                            Console.Write($"{product.ProductName}     ");
                        }
                        counter++;
                        Console.WriteLine();
                    }
                }

                if (orderDict.Count()==0)
                {
                    Console.WriteLine("Nothing to delete\n");
                    return this;
                }

                Console.Write("Choose Order what do you want delete from: ");
                int chooseOrderToDeleteId;
                if (!int.TryParse(Console.ReadLine(), out chooseOrderToDeleteId))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }

                OrderView choosedOrderToUpdate;
                if (!orderDict.TryGetValue(chooseOrderToDeleteId, out choosedOrderToUpdate))
                {
                    Console.WriteLine("Wrong number\n" +
                            "Try Again");
                    return this;
                }

                var productsToDelete = orderService.GetProductsByOrderId(choosedOrderToUpdate.OrderId);
                counter = 1;
                foreach (var product in productsToDelete)
                {
                    Console.Write($"{counter})    ");
                    Console.WriteLine($"{product.ProductName}     ");
                    counter++;
                }

                Console.Write("Choose Product what do you want delete from: ");
                int chooseProductToDeleteId;
                if (int.TryParse(Console.ReadLine(), out chooseProductToDeleteId))
                {
                    if (!(chooseProductToDeleteId > 0) || !(chooseProductToDeleteId <= counter))
                    {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                    }
                }
                else
                {
                    Console.WriteLine("Wrong number\n" +
                            "Try Again");
                    return this;
                }
                var productToDelete = productsToDelete.ToList()[chooseProductToDeleteId - 1];

                orderService.RemoveProductFromOrder(productToDelete.ProductId, choosedOrderToUpdate.OrderId);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteOrder()
        {
            try
            {
                CustomerView newCustomer = new CustomerView();
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                newCustomer.CustomerEmail = email;
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<CustomerView, CustomerDTO>();
                    cfg.CreateMap<OrderDTO, OrderView>();
                }).CreateMapper();

                var customerDTO = mapper.Map<CustomerView, CustomerDTO>(newCustomer);
                var customerDTOFromDB = customerService.GetUserByEmail(customerDTO.CustomerEmail);

                var ordersDTOFromDB = orderService.GetOrdersByCustomerId(customerDTOFromDB.CustomerId);
                var ordersViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(ordersDTOFromDB);

                int counter = 1;
                var orderDict = new Dictionary<int, OrderView>();
                foreach (var item in ordersViews)
                {
                    var products = orderService.GetProductsByOrderId(item.OrderId);
                    orderDict.Add(counter, item);
                    Console.Write($"{counter})");
                    if (products.Count() != 0)
                    {
                        foreach (var product in products)
                        {
                            Console.Write($"{product.ProductName}   ");
                        }
                        counter++;
                    }
                    else
                    {
                        Console.Write("None");
                        counter++;
                    }
                    Console.WriteLine();
                }

                if (orderDict.Count() == 0)
                {
                    Console.WriteLine("Nothing to delete\n");
                    return this;
                }

                Console.Write("Choose Order what do you want to delete: ");
                int chooseOrderToDeleteId;
                if (!int.TryParse(Console.ReadLine(), out chooseOrderToDeleteId))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }

                OrderView choosedOrderToUpdate;
                if (!orderDict.TryGetValue(chooseOrderToDeleteId, out choosedOrderToUpdate))
                {
                    Console.WriteLine("Wrong number\n" +
                            "Try Again");
                    return this;
                }

                orderService.DeleteOrderById(choosedOrderToUpdate.OrderId);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetAllCustomerOrders()
        {
            try
            {
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDTO, OrderView>();
                }).CreateMapper();

                var customerDTOFromDB = customerService.GetUserByEmail(email);
                var ordersDTOFromDB = orderService.GetOrdersByCustomerId(customerDTOFromDB.CustomerId);
                var ordersViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(ordersDTOFromDB);

                int counter = 1;
                foreach (var item in ordersViews)
                {
                    var products = orderService.GetProductsByOrderId(item.OrderId);
                    Console.Write($"{counter}) ");
                    if (products.Count() != 0)
                    {
                        foreach (var product in products)
                        {
                            Console.Write($"{product.ProductName}   ");
                        }
                        counter++;
                    }
                    else
                    {
                        Console.Write("None");
                        counter++;
                    }
                    Console.WriteLine();
                }

                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController AddNewProductToOrder()
        {
            try
            {
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDTO, OrderView>();
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();


                var customerDTOFromDB = customerService.GetUserByEmail(email);

                var ordersDTOFromDB = orderService.GetOrdersByCustomerId(customerDTOFromDB.CustomerId);
                var ordersViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(ordersDTOFromDB);

                int counter = 1;
                var orderDict = new Dictionary<int, OrderView>();
                foreach (var item in ordersViews)
                {
                    var products = orderService.GetProductsByOrderId(item.OrderId);
                    Console.Write($"{counter}) ");
                    orderDict.Add(counter, item);
                    if (products.Count() != 0)
                    {
                        foreach (var product in products)
                        {
                            Console.Write($"{product.ProductName}   ");
                        }
                        counter++;
                    }
                    else
                    {
                        Console.Write("None");
                        counter++;
                    }
                    Console.WriteLine();
                }

                Console.Write("Choose Order what do you want to add product: ");
                int chooseOrderToAddId;
                if (!int.TryParse(Console.ReadLine(), out chooseOrderToAddId))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }

                OrderView choosedOrderToUpdate;
                if (!orderDict.TryGetValue(chooseOrderToAddId, out choosedOrderToUpdate))
                {
                    Console.WriteLine("Wrong number\n" +
                            "Try Again");
                    return this;
                }

                Console.Write("Type product name: ");
                string productName = Console.ReadLine();
                ProductDTO productDTO = productService.GetProductByName(productName);
                if (productDTO==null)
                {
                    Console.WriteLine("Product doesn't exist\n" +
                            "Try Again");
                    return this;
                }
                ProductView productView = mapper.Map<ProductDTO, ProductView>(productDTO);

                orderService.AddProductToOrder(productView.ProductId, choosedOrderToUpdate.OrderId);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }
    }
}
