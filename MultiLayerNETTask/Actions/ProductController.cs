﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;
using MultiLayerNETTask.Infrastructure;
using MultiLayerNETTask.Interfaces;
using MultiLayerNETTask.ObjectsView;
using AutoMapper;
using BLL.ObjectsDTO;

namespace MultiLayerNETTask.Actions
{
    public class ProductController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        IProductService productService = Conf.GetService(typeof(IProductService)) as IProductService;

        public ProductController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = CreateProduct,
                [2] = UpdateProductInfo,
                [3] = DeleteProduct,
                [4] = GetProductByName,
                [5] = GetAllProducts,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Create product" +
                "\n2 - Update product info" +
                "\n3 - Delete product" +
                "\n4 - Get product by name" +
                "\n5 - Get all products" +
                "\n6 - Return");
        }

        public IController CreateProduct()
        {
            try
            {
                ProductView newProduct = new ProductView();
                Console.Write("Enter product name: ");
                newProduct.ProductName = Console.ReadLine();

                if (productService.GetProductByName(newProduct.ProductName) != null)
                {
                    Console.WriteLine("Product already exist\n" +
                       "Try Again");
                    return this;
                }

                Console.Write("Enter product price: ");
                int price;
                if (!int.TryParse(Console.ReadLine(), out price))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }
                newProduct.ProductPrice = price;
                Console.Write("Enter product count: ");
                int count;
                if (!int.TryParse(Console.ReadLine(), out count))
                {
                    Console.WriteLine("Wrong number\n" +
                        "Try Again");
                    return this;
                }
                newProduct.Count = count;

                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductView, ProductDTO>()).CreateMapper();


                productService.AddProductToDB(mapper.Map<ProductView, ProductDTO>(newProduct));
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController UpdateProductInfo()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ProductDTO, ProductView>();
                    cfg.CreateMap<ProductView, ProductDTO>();
                }).CreateMapper();

                Console.Write("Enter product name: ");
                var name = Console.ReadLine();
                var productDTO = productService.GetProductByName(name);
                if (productDTO == null)
                {
                    Console.WriteLine("Product doesn't exist\n" +
                       "Try Again");
                    return this;
                }

                var productView = mapper.Map<ProductDTO, ProductView>(productDTO);

                Console.WriteLine("Enter new name, else enter \"skip\"");
                var newValue = Console.ReadLine();
                if (!(newValue == "skip" || newValue == ""))
                    productView.ProductName = newValue;
                Console.WriteLine("Enter new count, else enter \"skip\"");
                newValue = Console.ReadLine();
                int newIntCountValue = 0;
                if (newValue != "skip")
                {
                    if (!int.TryParse(newValue, out newIntCountValue) || newIntCountValue < 0)
                    {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                    }

                    productView.Count = newIntCountValue;

                }
                Console.WriteLine("Enter new cost, else enter \"skip\"");
                newValue = Console.ReadLine();
                int newIntCostValue = 0;
                if (newValue != "skip")
                {
                    if (!int.TryParse(newValue, out newIntCostValue) || newIntCostValue < 0)
                    {
                        Console.WriteLine("Wrong number\n" +
                            "Try Again");
                        return this;
                    }
                    productView.ProductPrice = newIntCostValue;
                }


                productService.UpdateProduct(mapper.Map<ProductView, ProductDTO>(productView));
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController DeleteProduct()
        {
            try
            {
                Console.Write("Enter product name: ");
                var name = Console.ReadLine();
                var productDTO = productService.GetProductByName(name);
                if (productDTO == null)
                {
                    Console.WriteLine("Product doesn't exist\n" +
                       "Try Again");
                    return this;
                }

                productService.DeleteProduct(productDTO);
                Console.WriteLine("Success");
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController GetProductByName()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();

                Console.Write("Enter product name: ");
                var name = Console.ReadLine();
                var productDTO = productService.GetProductByName(name);
                if (productDTO == null)
                {
                    Console.WriteLine("Product doesn't exist\n" +
                       "Try Again");
                    return this;
                }

                var product = mapper.Map<ProductDTO, ProductView>(productDTO);

                Console.WriteLine("Product Id: " + product.ProductId);
                Console.WriteLine("Product name: " + product.ProductName);
                Console.WriteLine("Product price: " + product.ProductPrice);
                Console.WriteLine("Count: " + product.Count);
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetAllProducts()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();

                var productsDTO = productService.GetAllProducts();
                var productsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(productsDTO);

                foreach (var item in productsView)
                {
                    Console.WriteLine($"Id: {item.ProductId} Name:{item.ProductName} Price:{item.ProductPrice} Count:{item.Count}");
                }
                Console.WriteLine();
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }

    }
}
