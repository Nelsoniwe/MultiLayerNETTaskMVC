﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace MultiLayerNETTask.Infrastructure
{
    class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOrderService>().To<OrderService>();
            Bind<IProductService>().To<ProductService>();
            Bind<ICustomerService>().To<CustomerService>();
            Bind<IStoreHouseOrderService>().To<StoreHouseOrderService>();
        }
    }
}
