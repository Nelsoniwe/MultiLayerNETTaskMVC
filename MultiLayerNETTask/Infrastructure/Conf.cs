﻿using BLL.Infrastructure;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace MultiLayerNETTask.Infrastructure
{
    public static class Conf
    {
        private static readonly string ConnectionString = @"Server=localhost,1433;Database=NewDataBase;User ID=sa;Password=Str0ngPa$$w0rd";
        private static NinjectModule ninjectModule = new UOWModule(ConnectionString);
        private static NinjectModule serviceModule = new ServiceModule();
        private static readonly StandardKernel kernel = new StandardKernel(ninjectModule, serviceModule);

        public static object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
    }
}
