﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultiLayerNETTask.ObjectsView
{
    public class ProductView
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public int Count { get; set; }
    }
}
