﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultiLayerNETTask.ObjectsView
{
    public class CustomerView
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
    }
}
