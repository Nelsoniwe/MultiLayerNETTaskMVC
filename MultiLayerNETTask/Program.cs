﻿using System;
using System.Collections.Generic;
using DAL.Context;
using DAL.Objects;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Ninject.Modules;
using BLL.Infrastructure;
using Ninject;
using BLL.Interfaces;
using BLL.ObjectsDTO;
using BLL.Services;
using MultiLayerNETTask.Infrastructure;
using MultiLayerNETTask.Interfaces;
using MultiLayerNETTask.Actions;

namespace MultiLayerNETTask
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            IController choosedController = new BaseController();
            
            while (isRunning)
            {
                choosedController.ShowMenu();
                int number;
                if(Int32.TryParse(Console.ReadLine(),out number) && choosedController.Functions.ContainsKey(number))
                {
                    choosedController = choosedController.Functions[number].Invoke();
                }
                else
                {
                    Console.WriteLine("Wrong number");
                }
            }
        }
    }
}