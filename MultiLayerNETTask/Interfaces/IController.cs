﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultiLayerNETTask.Interfaces
{
    public interface IController
    {
        Dictionary<int, Func<IController>> Functions { get; set; }
        void ShowMenu();
    }
}
