﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using PLMVC.Infrastructure;
using AutoMapper;
using BLL.ObjectsDTO;
using PLMVC.Models;

namespace PLMVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        public CustomerController()
        {
            customerService = Conf.GetService(typeof(ICustomerService)) as ICustomerService;
        }

        public ActionResult GetAll()
        {
            var customers = customerService.GetAllCustomers();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
            var customersView = mapper.Map<IEnumerable<CustomerDTO>, IEnumerable<CustomerView>>(customers);
            ViewBag.Customers = customersView;
            return View("ListOfCustomers");
        }

        public ActionResult FindCustomerByEmail()
        {
            return View("FindCustomerByEmail");
        }

        [HttpPost]
        public ActionResult FindCustomerByEmail(CustomerView customerViewInput)
        {
            try
            {
                var customer = customerService.GetUserByEmail(customerViewInput.CustomerEmail);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, CustomerView>()).CreateMapper();
                var customerView = mapper.Map<CustomerDTO, CustomerView>(customer);
                ViewBag.Customers = new List<CustomerView>() { customerView };
                return View("ListOfCustomers");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult AddCustomerFields()
        {
            return View();
        }


        [HttpPost]
        public IActionResult AddCustomerFields(CustomerView customerView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerView, CustomerDTO>()).CreateMapper();
                var customersDTO = mapper.Map<CustomerView, CustomerDTO>(customerView);
                customerService.AddCustomerToDB(customersDTO);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult EditCustomerFields()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditCustomerFields(int id, CustomerView customerView)
        {
            try
            {
                customerView.CustomerId = id;
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CustomerView, CustomerDTO>()).CreateMapper();
                var customersDTO = mapper.Map<CustomerView, CustomerDTO>(customerView);
                customerService.UpdateCustomer(customersDTO);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult DeleteCustomer(int id)
        {
            try
            {
                var customersDTO = customerService.GetUserById(id);
                customerService.DeleteCustomer(customersDTO);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }
    }
}
