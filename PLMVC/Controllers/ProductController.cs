﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.ObjectsDTO;
using BLL.Services;
using PLMVC.Infrastructure;
using PLMVC.Models;

namespace PLMVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService productsService;

        public ProductController()
        {
            productsService = Conf.GetService(typeof(ProductService)) as ProductService;
        }

        public ActionResult GetAll()
        {
            var products = productsService.GetAllProducts();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, ProductView>()).CreateMapper();
            var productsView = mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(products);
            ViewBag.Products = productsView;
            return View("ListOfProducts");
        }

        public ActionResult FindProductByName()
        {
            return View("FindProductByName");
        }

        [HttpPost]
        public ActionResult FindProductByName(ProductView productViewInput)
        {
            try
            {
                var product = productsService.GetProductByName(productViewInput.ProductName);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductDTO, ProductView>()).CreateMapper();
                var productView = mapper.Map<ProductDTO, ProductView>(product);
                ViewBag.Products = new List<ProductView>() { productView };
                return View("ListOfProducts");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult AddProductFields()
        {
            return View();
        }


        [HttpPost]
        public IActionResult AddProductFields(ProductView productView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductView, ProductDTO>()).CreateMapper();
                var productDto = mapper.Map<ProductView, ProductDTO>(productView);
                productsService.AddProductToDB(productDto);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult DeleteProduct(int id)
        {
            try
            {
                productsService.DeleteProductById(id);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }
        public ActionResult EditProductFields()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditProductFields(int id, ProductView productView)
        {
            try
            {
                productView.ProductId = id;
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductView, ProductDTO>()).CreateMapper();
                var productDTO = mapper.Map<ProductView, ProductDTO>(productView);
                productsService.UpdateProduct(productDTO);
                return RedirectToAction("GetAll");
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }
    }
}
