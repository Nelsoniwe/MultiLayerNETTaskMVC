﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.ObjectsDTO;
using PLMVC.Models;
using BLL.Interfaces;
using PLMVC.Infrastructure;

namespace PLMVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;
        private readonly IProductService productService;
        public OrderController()
        {
            orderService = Conf.GetService(typeof(IOrderService)) as IOrderService;
            productService = Conf.GetService(typeof(IProductService)) as IProductService;
        }
        public ActionResult GetCustomerOrders(int id)
        {
            try
            {
                ViewBag.OrderCustomerId = id;

                var orders = orderService.GetOrdersByCustomerId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDTO, OrderView>();
                    cfg.CreateMap<ProductDTO, ProductView>();
                }).CreateMapper();
                var orderViews = mapper.Map<IEnumerable<OrderDTO>, IEnumerable<OrderView>>(orders);
                var orderProducts = new Dictionary<int, IEnumerable<ProductView>>();
                foreach (var order in orderViews)
                {
                    var products = orderService.GetProductsByOrderId(order.OrderId);
                    orderProducts.Add(order.OrderId,
                        mapper.Map<IEnumerable<ProductDTO>, IEnumerable<ProductView>>(products));
                }
                ViewBag.CustomerOrders = orderProducts;

                return View();
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                if (e.Message == "Customer hasn't any order")
                {
                    return View("NullOrdersException");
                }

                return View("ExceptionMessage");
            }
        }
        public ActionResult DeleteProductFromOrder(int customerId, int orderId, int productId)
        {
            try
            {
                orderService.RemoveProductFromOrder(productId, orderId);
                return RedirectToAction("GetCustomerOrders", new { id = customerId });
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult AddProductToOrder(int customerId, int orderId)
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProductToOrder(int customerId, int orderId, ProductView productView)
        {
            try
            {
                orderService.AddProductToOrder(productService.GetProductByName(productView.ProductName).ProductId, orderId);
                return RedirectToAction("GetCustomerOrders", new { id = customerId });
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

        public ActionResult DeleteOrder(int customerId, int orderId)
        {
            try
            {
                orderService.DeleteOrderById(orderId);
                return RedirectToAction("GetCustomerOrders", new { id = customerId });
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }


        public ActionResult CreateOrder(int customerId)
        {
            try
            {
                orderService.CreateOrder(new OrderDTO() { CustomerId = customerId });
                return RedirectToAction("GetCustomerOrders", new { id = customerId });
            }
            catch (Exception e)
            {
                ViewData["ExceptionMessage"] = e.Message;
                return View("ExceptionMessage");
            }
        }

    }
}
