﻿namespace PLMVC.Models
{
    public class CustomerView
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
    }
}
