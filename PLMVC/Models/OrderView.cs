﻿namespace PLMVC.Models
{
    public class OrderView
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
    }
}
